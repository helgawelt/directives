var dirApp = angular.module('dirApp', ['angularResizable']);

dirApp.directive("myButton", function () {
    return {
        restrict: 'E',
        replace: false,
        template: '<button ng-click="sayOk()">Click me</button>',
        link: function($scope) {
            $scope.sayOk = function() {
                console.log('ok');
            }
        }
    }
});
